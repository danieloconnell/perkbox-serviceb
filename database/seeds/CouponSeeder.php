<?php

use Illuminate\Database\Seeder;

class CouponSeeder extends Seeder
{
    /**
     * Coupon brands
     */
    protected $brands = [
        'Tesco',
        'Sainsburys',
        'Asda',
        'Co-Op',
        'Waitrose',
        'Aldi',
        'Lidl'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect($this->brands)->each(function($item, $key) {
            DB::table('coupons')->insert([
                'brand'  => $item,
                'value'  => rand(10, 50)
            ]);
        });
    }
}
