# Lumen based, single endpoint, Coupon API

To set up the project, clone and run composer install to fetch dependencies. Following this the database needs to be populated, to do this run:

```php artisan migrate --seed```

This will create the coupon table and seed it from a list of brands and randomised values. 

To repopulate the database after calling the API a few times and clearing the Coupon table, you can use:

```php artisan db:seed``` 

The authorization token for making API calls can be found .env file as API_KEY.

The optional filter parameters are limit, brand and value. 

Run using the PHP server with:

```php -S localhost:8000 -t public```