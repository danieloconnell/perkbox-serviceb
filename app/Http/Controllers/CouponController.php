<?php

namespace App\Http\Controllers;

use App\Coupon;

class CouponController extends Controller
{
    /**
     * Get the requested coupons and remove them from the database
     * 
     * @return Illuminate\Http\JsonResponse
     */
    public function getCoupons() : string
    {
        $coupons = Coupon::all();

        // Brand filter
        if(isset($_GET['brand'])) {
            $coupons = $coupons->where('brand', $_GET['brand']);
        }

        // Value filter
        if(isset($_GET['value'])) {
            $coupons = $coupons->where('value', $_GET['value']);
        }

        // Limit filter
        if(isset($_GET['limit'])) {
            $coupons = $coupons->take($_GET['limit']);
        }

        collect($coupons)->each(function($item, $key) {
            Coupon::where('brand', $item->brand)
                  ->where('value', $item->value)
                  ->delete();
        });

        return $coupons->toJson();
    }
}
